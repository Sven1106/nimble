﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Events;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Nimble.Common.Actors
{
    public class UserManagerActor : ReceiveActor // Should be a persistent actor
    {
        private Dictionary<string, IActorRef> UsersByEmail { get; set; } = new Dictionary<string, IActorRef>();
        private Dictionary<Guid, IActorRef> UsersById { get; set; } = new Dictionary<Guid, IActorRef>();

        private void Ready()
        {
            Receive<RegisterUser>(message =>
            {
                string email = message.Email.Trim();
                if (UsersByEmail.ContainsKey(email) == false)
                {
                    Guid userId = message.Id;
                    IActorRef user = Context.ActorOf(Props.Create(() => new UserActor(userId, message.Email, message.Password)), userId.ToString());
                    UsersByEmail.Add(email, user);
                    UsersById.Add(userId, user);
                }
                else
                {
                    // User already exists
                }
            });

            Receive<AuthenticateUser>(message =>
            {
                string email = message.Email.Trim();
                if (UsersByEmail.TryGetValue(email, out IActorRef user))
                {
                    user.Forward(message);
                }
                else
                {
                    // User doesn't exist
                }
            });
        }

        #region Lifecycle Hooks
        protected override void PreStart()
        {
            Become(Ready);
        }

        protected override void PostStop()
        {

        }
        #endregion
    }
}
