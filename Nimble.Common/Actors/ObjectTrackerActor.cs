﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json.Linq;
using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Models.Deserialization;
using Nimble.Common.Models;
using Nimble.Common.Events;

namespace Nimble.Common.Actors
{
    public class ObjectTrackerActor : ReceiveActor // TODO Should be persistent actor.
    {
        private Dictionary<string, JArray> ObjectsByScraperSchemaName { get; set; } = new Dictionary<string, JArray>();
        public ObjectTrackerActor(List<ScraperSchema> scraperSchemas)
        {
            scraperSchemas.ForEach((scraperSchema) =>
            {
                ObjectsByScraperSchemaName.Add(scraperSchema.Name, new JArray());
            });
            Ready();
        }

        private void Ready()
        {
            Receive<ObjectContentFound>(message => // ObjectValidator
            {
                //TODO Check if object already exists
                ObjectsByScraperSchemaName[message.ScraperSchemaName].Add(message.JObject);
                NimbleActorPaths.DirectoryRef.Tell(message);
            });
        }

    }
}
