﻿using Akka.Actor;
using Akka.Dispatch;
using Nimble.Common.Commands;
using Nimble.Common.Models;
using PuppeteerSharp;
using System;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Linq;

namespace Nimble.Common.Actors
{
    public class BrowserActor : ReceiveActor
    {
        private int MaxAmountOfPages { get; set; }
        private Browser ChromiumBrowserInstance { get; set; }
        private XpathConfigurationForPuppeteer XpathConfiguration { get; set; }
        private ConcurrentQueue<DownloadUrl> UncrawledUrls { get; set; } = new ConcurrentQueue<DownloadUrl>(); // TODO Should this be moved to the Urltracker?
        private ConcurrentQueue<IActorRef> IdleChildren { get; set; } = new ConcurrentQueue<IActorRef>(); // Why was this in PrepareBrowserInstanceAsync?
        private IActorRef ProgressRef { get; set; }
        private IActorRef HtmlContentGuardRef { get; set; }
        public BrowserActor(XpathConfigurationForPuppeteer xpathConfiguration)
        {
            XpathConfiguration = xpathConfiguration;
            MaxAmountOfPages = 3;

                                                               // TODO Add Chromium instance handling
                                                               // TODO Add Chromium instance handling
                                                               // TODO Add Chromium instance handling
                                                               // TODO Add Chromium instance handling
                                                               // TODO Add Chromium instance handling
            Configurable();
        }
        private void Configurable()
        {
            Receive<ConfigureBrowser>(message => {
                ProgressRef = message.ProgressRef;
                HtmlContentGuardRef = message.HtmlContentGuardRef;
                ActorTaskScheduler.RunTask(async () =>
                {
                    await PrepareBrowserInstanceAsync();
                    Become(Working);
                });
            });
        }

        private void Idle()
        {
            Receive<DownloadUrl>(url =>
            {
                UncrawledUrls.Enqueue(url);
            });

            Receive<RequestWork>(message =>
            {
                Sender.Forward(PoisonPill.Instance);
            });

            Receive<Start>(message =>
            {
                CreateDefaultTabActors();
                Become(Working);
            });
            Receive<Pause>((message) =>
            {

            });
        }

        private void Working()
        {
            Receive<DownloadUrl>(url =>
            {
                if (IdleChildren.TryDequeue(out IActorRef pageActor))
                {
                    pageActor.Tell(url);
                }
                else
                {
                    UncrawledUrls.Enqueue(url);
                }
            });

            Receive<RequestWork>(message =>
            {
                if (UncrawledUrls.TryDequeue(out DownloadUrl url))
                {
                    Sender.Tell(url);
                }
                else
                {
                    IdleChildren.Enqueue(Sender);
                }
            });
            Receive<Start>(message =>
            {

            });
            Receive<Pause>((message) =>
            {
                while (IdleChildren.TryDequeue(out IActorRef tabActor)) // we only kill tabs that are Idle to ensure that the working tabs finishes their work.
                {
                    tabActor.Forward(PoisonPill.Instance);
                }
                Become(Idle);
            });
        }

        private async Task PrepareBrowserInstanceAsync()
        {
            // TODO Add integration to browserless
            // TODO Add integration to docker
            await new BrowserFetcher().DownloadAsync(BrowserFetcher.DefaultRevision);
            var args = new string[] {
                    "--no-sandbox",
                    "--disable-plugins",
                    "--disable-sync",
                    "--disable-gpu",
                    "--disable-speech-api",
                    "--disable-remote-fonts",
                    "--disable-shared-workers",
                    "--disable-webgl",
                    "--no-experiments",
                    "--no-first-run",
                    "--no-default-browser-check",
                    "--no-wifi",
                    "--no-pings",
                    "--no-service-autorun",
                    "--disable-databases",
                    "--disable-default-apps",
                    "--disable-demo-mode",
                    "--disable-notifications",
                    "--disable-permissions-api",
                    "--disable-background-networking",
                    "--disable-3d-apis",
                    "--disable-bundled-ppapi-flash"
                };
            var launchOptions = new LaunchOptions { Headless = false, Args = args, IgnoreHTTPSErrors = false }; // Setting Headless=false will prevent Page.ClickAsync to work if page is not in focus. https://github.com/puppeteer/puppeteer/issues/3339
            ChromiumBrowserInstance = await Puppeteer.LaunchAsync(launchOptions);
            // ChromiumBrowserInstance = await Puppeteer.ConnectAsync(new ConnectOptions() { BrowserWSEndpoint = "wss://chrome.browserless.io", IgnoreHTTPSErrors = true });
            CreateDefaultTabActors();
        }
        private void CreateDefaultTabActors()
        {
            int currentAmountOfPages = Context.GetChildren().Count();
            for (int i = 0; i < MaxAmountOfPages - currentAmountOfPages; i++)
            {
                CreateTabActor(ChromiumBrowserInstance.WebSocketEndpoint);
            }
        }

        private void CreateTabActor(string webSocketUrl)
        {
            Context.ActorOf(Props.Create(() => new TabActor(webSocketUrl, XpathConfiguration, ProgressRef, HtmlContentGuardRef)));
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {

            return new OneForOneStrategy(
                exception =>
                {
                    if (exception is PuppeteerException)
                    {
                        if (exception is ProcessException)
                        {

                        }
                        else if (exception is EvaluationFailedException)
                        {

                        }
                        else if (exception is MessageException)
                        {

                        }
                        else if (exception is NavigationException) // Couldn't Navigate to url. Or Browser was disconnected //Target.detachedFromTarget
                        {
                            return Directive.Restart;
                        }
                        else if (exception is SelectorException)
                        {

                        }
                        else if (exception is TargetClosedException) // Page was closed
                        {
                            return Directive.Restart;
                        }
                    }
                    else if (exception is NullReferenceException)
                    {
                        return Directive.Escalate;
                    }
                    return Directive.Resume;
                });
        }

        #region Lifecycle Hooks
        protected override void Unhandled(object message)
        {
            base.Unhandled(message);
        }
        protected override void PreStart()
        {

        }

        protected override void PostStop()
        {
            if (ChromiumBrowserInstance != null)
            {
                ChromiumBrowserInstance.Dispose();
            }
        }
        #endregion
    }
}
