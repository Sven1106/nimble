﻿using Akka.Actor;
using Nimble.Common.Commands;
using System;
using System.Collections.Generic;
using Nimble.Common.Events;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Schema;
using Nimble.Common.Models;
using Nimble.Common.Models.Deserialization;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

namespace Nimble.Common.Actors
{
    public class UserActor : ReceiveActor
    {
        private Guid Id { get; set; }
        //TODO Add Public exposed ID for JWT?
        private string Email { get; set; }
        private byte[] PasswordHash { get; set; }
        private byte[] PasswordSalt { get; set; }
        private Dictionary<Guid, IActorRef> Projects { get; } = new Dictionary<Guid, IActorRef>(); // Persist
        public UserActor(Guid id, string email, string password)
        {
            Id = id;
            Email = email;
            CreatePasswordHash(password, out byte[] passwordHash, out byte[] passwordSalt);
            PasswordHash = passwordHash;
            PasswordSalt = passwordSalt;
        }
        // TODO Add         private void InActive(), Return UserActivated
        private void Active()
        {
            #region Commands
            Receive<RegisterProject>(message =>
            {
                ProjectDefinition projectDefinition = DeserializeToProjectDefinition(message.ProjectDefinitionAsJson);
                Guid projectId = projectDefinition.ProjectId;
                if (Projects.ContainsKey(projectId) == false)
                {
                    IActorRef newProject = Context.ActorOf(Props.Create(() => new ProjectActor(Id, projectDefinition)), projectId.ToString());
                    Projects.Add(projectId, newProject);
                }
                else
                {
                    // Project already exists.
                }
            });

            Receive<AuthenticateUser>(message =>
            {
                if (VerifyPasswordHash(message.Password, PasswordHash, PasswordSalt))
                {
                    // Send UserAuthenticated
                    NimbleActorPaths.DirectoryRef.Tell(new UserAuthenticated(Id,Email), Self);
                }
                else
                {
                    // Username & Password was wrong.
                }
            });


            #region forwarding messages down the hierachy wont bottleneck the system since the messages will only happen as user input.
            Receive<PauseProject>(message =>
            {
                Guid projectId = message.ProjectId;
                if (Projects.TryGetValue(projectId, out IActorRef project))
                {
                    project.Forward(new Pause());
                }
            });
            Receive<StartProject>(message =>
            {
                Guid projectId = message.ProjectId;
                if (Projects.TryGetValue(projectId, out IActorRef project))
                {
                    project.Forward(new Start());
                }
            });

            Receive<TerminateProject>(message =>
            {
                Guid projectId = message.ProjectId;
                if (Projects.TryGetValue(projectId, out IActorRef project))
                {
                    project.Forward(new Terminate());
                }
            });
            #endregion

            #endregion

            #region Events
            Receive<ProjectTerminated>(message =>
            {
                Guid projectId = Guid.Parse(Sender.Path.Name);
                if (Projects.TryGetValue(projectId, out IActorRef project))
                {
                    Projects.Remove(projectId);
                }
            });
            #endregion
        }

        #region Lifecycle Hooks
        protected override void PreStart()
        {
            NimbleActorPaths.DirectoryRef.Tell(new AnnounceSelf(Id.ToString()), Self);
            NimbleActorPaths.DirectoryRef.Tell(new UserRegistered(Id), Self);
            Become(Active);
        }

        protected override void PostStop()
        {

        }

        #endregion
        private ProjectDefinition DeserializeToProjectDefinition(string json)
        {
            JsonTextReader reader = new JsonTextReader(new StringReader(json));
            JSchemaValidatingReader jSchemaReader = new JSchemaValidatingReader(reader)
            {
                Schema = JSchema.Parse(File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "ProjectDefinitionSchema.json")))
            };
            IList<string> errorMessages = new List<string>();
            jSchemaReader.ValidationEventHandler += (o, a) => errorMessages.Add(a.Message);
            JsonSerializer serializer = new JsonSerializer();
            ProjectDefinition projectDefinition = serializer.Deserialize<ProjectDefinition>(jSchemaReader);
            if (errorMessages.Count > 0)
            {
                foreach (var eventMessage in errorMessages)
                {
                    Console.WriteLine(eventMessage);
                }
                throw new Exception("json was not valid");
            }
            return projectDefinition;
        }
        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));
            }
        }
        private bool VerifyPasswordHash(string password, byte[] passwordHash, byte[] passwordSalt)
        {
            using (var hmac = new System.Security.Cryptography.HMACSHA512(passwordSalt))
            {
                var computedHash = hmac.ComputeHash(Encoding.UTF8.GetBytes(password));

                for (int i = 0; i < computedHash.Length; i++)
                {
                    if (computedHash[i] != passwordHash[i])
                    {
                        return false;
                    }
                }
            }
            return true;
        }


        //public static string CreateUserToken(string id, string name, string email, string role, IConfiguration config) // Move Creation of userToken to API
        //{

        //    var claims = new[] {
        //        new Claim(ClaimTypes.NameIdentifier, id),
        //        new Claim(ClaimTypes.Name, name),
        //        new Claim(ClaimTypes.Email, email)
        //    };

        //    var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.GetSection("SigningKey").Value));

        //    var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

        //    var tokenDescriptor = new SecurityTokenDescriptor
        //    {
        //        Subject = new ClaimsIdentity(claims),
        //        Expires = DateTime.UtcNow.AddSeconds(1),
        //        SigningCredentials = credentials
        //    };

        //    var jwtTokenHandler = new JwtSecurityTokenHandler();

        //    var createdToken = jwtTokenHandler.CreateToken(tokenDescriptor);
        //    return jwtTokenHandler.WriteToken(createdToken);
        //}
    }
}
