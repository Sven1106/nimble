﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Models;
using Nimble.Common.Models.Deserialization;
using HtmlAgilityPack;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Xml.XPath;
using Nimble.Common.Events;

namespace Nimble.Common.Actors
{
    public class ObjectParserActor : ReceiveActor
    {
        private Guid AccountId { get; }
        private Guid ProjectId { get; }
        private List<ScraperSchema> ScraperSchemas { get; set; }
        private readonly bool showDebug = false;
        private IActorRef ObjectTrackerRef { get; set; }
        public ObjectParserActor(List<ScraperSchema> scraperSchemas, Guid accountId, Guid projectId)
        {
            AccountId = accountId;
            ProjectId = projectId;
            ScraperSchemas = scraperSchemas;
            Configurable();
        }
        private void Configurable()
        {
            Receive<ConfigureObjectParser>(message =>
            {
                ObjectTrackerRef = message.ObjectTrackerRef;
                Become(Ready);
            });

        }
        private void Ready()
        {
            Receive<HtmlContentFound>(htmlContent =>
            {
                if (showDebug)
                {
                    ColorConsole.WriteLine($"{Self.Path.Name} started parsing objects from: {htmlContent.SourceUrl}", ConsoleColor.White);
                }
                HtmlDocument htmlDoc = new HtmlDocument();
                htmlDoc.LoadHtml(htmlContent.Html);
                HtmlNode documentNode = htmlDoc.DocumentNode;
                foreach (ScraperSchema scraperSchema in ScraperSchemas) // TODO Find a better way to reference the job schemas
                {
                    JObject objectFound = new JObject();
                    foreach (NodeAttribute node in scraperSchema.Nodes)
                    {
                        JToken value = GetValueForJTokenRecursively(node, documentNode);
                        if (value.ToString() == "")
                        {
                            continue;
                        }
                        objectFound.Add(node.Name, value);
                        Metadata metadata = new Metadata(htmlContent.SourceUrl.ToString(), DateTime.UtcNow);
                        objectFound.Add("metadata", JObject.FromObject(metadata, new JsonSerializer()
                        {
                            ContractResolver = new CamelCasePropertyNamesContractResolver()
                        }));
                    }
                    if (objectFound.HasValues == false)
                    {
                        continue;
                    }
                    ObjectContentFound scrapedObjectContent = new ObjectContentFound(AccountId, ProjectId, scraperSchema.Name, objectFound);
                    ObjectTrackerRef.Tell(scrapedObjectContent);
                }
                if (showDebug)
                {
                    ColorConsole.WriteLine($"{Self.Path.Name} finished parsing objects from: {htmlContent.SourceUrl}", ConsoleColor.White);
                }
            });
        }
        private JToken GetValueForJTokenRecursively(NodeAttribute node, HtmlNode htmlNode) // TODO: see if it is possible to use the same HTMLNode/Htmldocument through out the extractions.
        {
            JToken jToken = "";
            if (node.GetMultipleFromPage)
            {
                JArray jArray = new JArray();
                if (node.Type == NodeType.String || node.Type == NodeType.Number || node.Type == NodeType.Boolean) // basic types
                {
                    HtmlNodeCollection elements = htmlNode.SelectNodes(node.Xpath);
                    if (elements != null)
                    {
                        foreach (var element in elements)
                        {
                            HtmlNodeNavigator navigator = (HtmlNodeNavigator)element.CreateNavigator();
                            if (navigator.Value.Trim() == "")
                            {
                                continue;
                            }
                            jArray.Add(navigator.Value.Trim());
                        }
                        jToken = jArray;
                    }
                }
                else if (node.Type == NodeType.Object && node.Attributes.Count > 0) // complex types
                {
                    JObject jObject = new JObject();
                    HtmlNodeCollection elements = htmlNode.SelectNodes(node.Xpath);
                    if (elements != null)
                    {
                        foreach (var element in elements)
                        {
                            foreach (var attribute in node.Attributes)
                            {
                                JToken value = GetValueForJTokenRecursively(attribute, element);
                                if (value.ToString() == "" && attribute.IsRequired)
                                {
                                    return jToken;
                                }
                                jObject.Add(attribute.Name, value);
                            }
                            jArray.Add(jObject);
                        }
                        jToken = jArray;
                    }
                }
            }
            else
            {
                HtmlNodeNavigator navigator = (HtmlNodeNavigator)htmlNode.CreateNavigator();
                if (node.Type == NodeType.String || node.Type == NodeType.Number || node.Type == NodeType.Boolean) // basic types
                {
                    XPathNavigator nodeFound = navigator.SelectSingleNode(node.Xpath);
                    // Get as Type
                    if (nodeFound != null)
                    {
                        if (nodeFound.Value.Trim() == "")
                        {
                            return jToken;
                        }
                        jToken = nodeFound.Value.Trim();
                    }
                }
                else if (node.Type == NodeType.Object && node.Attributes.Count > 0) // complex types
                {
                    HtmlNode element = htmlNode.SelectSingleNode(node.Xpath);
                    if (element != null)
                    {
                        JObject jObject = new JObject();
                        foreach (var attribute in node.Attributes)
                        {
                            JToken value = GetValueForJTokenRecursively(attribute, element);
                            if (value.ToString() == "" && attribute.IsRequired)
                            {
                                return jToken;
                            }
                            jObject.Add(attribute.Name, value);
                        }
                        jToken = jObject;
                    }
                }
            }
            return jToken;
        }
    }
}
