﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Akka.Actor;
using Akka.Dispatch;
using Nimble.Common.Commands;
using Nimble.Common.Events;
using Nimble.Common.Models;
using PuppeteerSharp;

namespace Nimble.Common.Actors
{
    public class TabActor : ReceiveActor
    {
        private Page ChromiumPageInstance { get; set; }
        private string WebSocket { get; set; }
        private XpathConfigurationForPuppeteer Configuration { get; set; }
        private IActorRef ProgressRef { get; }
        private IActorRef HtmlContentGuardRef { get; }
        public TabActor(string webSocket, XpathConfigurationForPuppeteer configuration, IActorRef progressRef, IActorRef htmlContentGuardRef)
        {
            WebSocket = webSocket;
            Configuration = configuration;
            ProgressRef = progressRef;
            HtmlContentGuardRef = htmlContentGuardRef;
        }

        private void PreparePage()
        {
            ActorTaskScheduler.RunTask(async () =>
            {
                await PreparePageAsync();
                Context.Parent.Tell(new RequestWork());
                Become(Idle);
            });
        }

        private void Idle()
        {
            Receive<DownloadUrl>(message =>
            {
                Become(Working);
                Self.Forward(message);
            });
        }

        private void Working()
        {
            Receive<DownloadUrl>(message =>
            {
                GetHtmlContentAsync(message.TargetUrl).PipeTo(Self, failure: exception => new ExceptionOccured(exception));
            });
            Receive<HtmlContentFound>(message =>
            {
                ProgressRef.Tell(new UrlWasProcessed(message.SourceUrl)); // Register that the url has been processed.
                HtmlContentGuardRef.Tell(message);
                Context.Parent.Tell(new RequestWork());
                Become(Idle);
            });
            Receive<ExceptionOccured>(message =>
            {
                if (message.Exception.InnerException is NavigationException navigationException && navigationException.Url != null)
                {
                    ProgressRef.Tell(new UrlWasProcessed(new Uri(navigationException.Url))); // Register that the url has been processed.
                    Context.Parent.Tell(new RequestWork());
                    Become(Idle);
                }
                else
                {
                    throw message.Exception;
                }
            });
        }

        private async Task PreparePageAsync()
        {
            PuppeteerSharp.Browser browser = await Puppeteer.ConnectAsync(new ConnectOptions() { BrowserWSEndpoint = WebSocket, IgnoreHTTPSErrors = true });
            ChromiumPageInstance = await browser.NewPageAsync();
            await ChromiumPageInstance.SetViewportAsync(new ViewPortOptions() { Width = 1920, Height = 1080 });
            await ChromiumPageInstance.SetRequestInterceptionAsync(true); // Intercepting the page seems to finish it prematurely
            ChromiumPageInstance.Request += async (sender, e) =>
            {
                try
                {
                    switch (e.Request.ResourceType)
                    {
                        case ResourceType.Font:
                        case ResourceType.EventSource:
                        case ResourceType.Image:
                        case ResourceType.Manifest:
                        case ResourceType.Media:
                        case ResourceType.Other:
                        case ResourceType.Ping:
                        case ResourceType.TextTrack:
                        case ResourceType.Unknown:
                            await e.Request.AbortAsync();
                            break;
                        case ResourceType.StyleSheet:
                        case ResourceType.Document:
                        case ResourceType.Fetch:
                        case ResourceType.Script:
                        case ResourceType.WebSocket:
                        case ResourceType.Xhr:
                        default:
                            await e.Request.ContinueAsync();
                            break;
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"Error => {ex.Message}");
                    await e.Request.ContinueAsync();
                }
            };
        }
        private async Task<HtmlContentFound> GetHtmlContentAsync(Uri url)
        {
            try
            {
                var response = await ChromiumPageInstance.GoToAsync(url.ToString(), 0, new WaitUntilNavigation[] { WaitUntilNavigation.Networkidle0 });

                await InvokeXpathConfigurationAsync();
                string html = await ChromiumPageInstance.GetContentAsync();
                return new HtmlContentFound(url, html);
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        private async Task InvokeXpathConfigurationAsync() // TODO REFACTOR. THIS MESSES UP THE STACK TRACE ON EXCEPTIONS 
        {
            ElementHandle acceptCookiesButton = await ScrollToXpathAndReturnElementHandleAsync(Configuration.XpathForAcceptCookiesButton);
            if (acceptCookiesButton != null)
            {
                try
                {
                    await ChromiumPageInstance.EvaluateExpressionAsync("document.evaluate('" + Configuration.XpathForAcceptCookiesButton + "',document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();");
                }
                catch (WaitTaskTimeoutException ex) // Only catches WaitTaskTimeoutException exceptions.
                {

                }
            }
            ElementHandle loadMoreButton = await ScrollToXpathAndReturnElementHandleAsync(Configuration.XpathForLoadMoreButton);
            if (loadMoreButton != null)
            {
                bool hasMoreContent = true;
                while (hasMoreContent)
                {
                    try
                    {
                        await ChromiumPageInstance.EvaluateExpressionAsync("document.evaluate('" + Configuration.XpathForLoadMoreButton + "',document, null, XPathResult.FIRST_ORDERED_NODE_TYPE, null).singleNodeValue.click();"); //Used this instead of ClickAsync() since it wont execute if page is not focused.
                    }
                    catch (WaitTaskTimeoutException ex) // Only catches WaitTaskTimeoutException exceptions.
                    {
                        hasMoreContent = false;
                    }
                }
            }
            foreach (var xpath in Configuration.AbsoluteXpathsForElementsToEnsureExist)
            {
                ElementHandle isElementVisible = await ScrollToXpathAndReturnElementHandleAsync(xpath);
                if (isElementVisible == null) // xpathsForDesiredHtmlElements has a treestructure, so we want to break out of the loop as soon as possible when not found.
                {
                    break;
                }
            }
        }
        private async Task<ElementHandle> ScrollToXpathAndReturnElementHandleAsync(string xpath) // TODO REFACTOR
        {
            if (xpath != null)
            {
                int attempt = 1;
                int attemptLimit = 3; // TODO Should be configurable
                int currentHeight = (int)await ChromiumPageInstance.EvaluateExpressionAsync("document.body.scrollHeight");
                while (attempt <= attemptLimit)
                {
                    try
                    {

                        return await ChromiumPageInstance.WaitForXPathAsync(xpath, new WaitForSelectorOptions { Visible = true, Timeout = 2500 });
                    }
                    catch (WaitTaskTimeoutException ex) // Only catches WaitTaskTimeoutException exceptions.
                    {
                        int newHeight = (int)await ChromiumPageInstance.EvaluateExpressionAsync("document.body.scrollHeight");
                        await ChromiumPageInstance.EvaluateExpressionAsync("window.scrollBy({top:" + newHeight + ",behavior:'smooth'})"); // To trigger autoload of new content.
                        if (currentHeight == newHeight)
                        {
                            break;
                        }
                        currentHeight = newHeight;
                    }
                    attempt++;
                }
            }

            return null;
        }

        #region Lifecycle Hooks
        protected override void PreStart()
        {
            PreparePage();
        }
        protected override void PreRestart(Exception reason, object message)
        {
            Self.Tell(message); // Is this the correct way to handle Crashes?
            base.PreRestart(reason, message);
        }

        protected override void PostStop()
        {
            if (ChromiumPageInstance != null)
            {
                ChromiumPageInstance.Dispose();
            }
        }

        #endregion
    }
}
