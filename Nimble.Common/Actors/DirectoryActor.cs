﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Events;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Actors
{
    public class DirectoryActor : ReceiveActor // This actor is an attempt to avoid having to traverse a message through all the parents of an actor.
        //TODO. Maybe the commands and events should be in their own actors for better scaling?!
    {
        private List<IActorRef> EventListeners { get; } = new List<IActorRef>();
        private ConcurrentDictionary<string, IActorRef> References { get; } = new ConcurrentDictionary<string, IActorRef>();
        private IActorRef AccountManagerRef { get; set; }
        public DirectoryActor()
        {
            RunTask(async () =>
            {
                AccountManagerRef = await Context.ActorSelection(NimbleActorPaths.UserManager.Path).ResolveOne(TimeSpan.FromSeconds(3));
            });

            #region Commands
            Receive<AnnounceSelf>(message => {
                References.TryAdd(message.Key, Sender);
            });

            Receive<WithdrawSelf>(message => {
                References.TryRemove(message.Key, out IActorRef reference);
            });

            Receive<SubscribeToEventListeners>(message => {
                EventListeners.Add(Sender);
            });

            Receive<UnsubscribeFromEventListeners>(message => {
                EventListeners.Remove(Sender);
            });

            //TODO abandon signoff ?? EDIT 2021-03-10 What does this mean?

            Receive<RegisterUser>(message =>
            {
                AccountManagerRef.Tell(message);
            });

            Receive<AuthenticateUser>(message =>
            {
                AccountManagerRef.Tell(message);
            });

            Receive<RegisterProject>(message =>
            {
                string key = message.AccountId.ToString();
                if (References.TryGetValue(key, out IActorRef reference))
                {
                    reference.Tell(message);
                }
            });
            Receive<StartProject>(message =>
            {
                string key = message.AccountId.ToString();
                if (References.TryGetValue(key, out IActorRef reference))
                {
                    reference.Tell(message);
                }
            });
            Receive<PauseProject>(message =>
            {
                string key = message.AccountId.ToString();
                if (References.TryGetValue(key, out IActorRef reference))
                {
                    reference.Tell(message);
                }
            });
            Receive<TerminateProject>(message =>
            {
                string key = message.AccountId.ToString();
                if (References.TryGetValue(key, out IActorRef reference))
                {
                    reference.Tell(message);
                }
            });
            #endregion

            #region Events
            // Replace all with a single Receive<IEvent>
            Receive<UserRegistered>(message =>
            {
                EventListeners.ForEach(eventlistener => {
                    eventlistener.Forward(message);
                });
            });

            Receive<UserAuthenticated>(message =>
            {
                EventListeners.ForEach(eventlistener => {
                    eventlistener.Forward(message);
                });
            });

            Receive<ProjectRegistered>(message =>
            {
                EventListeners.ForEach(eventlistener => {
                    eventlistener.Forward(message);
                });
            });

            Receive<ProjectTerminated>(message =>
            {
                EventListeners.ForEach(eventlistener => {
                    eventlistener.Forward(message);
                });
            });

            Receive<ProjectProgress>(message =>
            {
                EventListeners.ForEach(eventlistener => {
                    eventlistener.Forward(message);
                });
            });

            Receive<ObjectContentFound>(message =>
            {
                EventListeners.ForEach(eventlistener => {
                    eventlistener.Forward(message);
                });
            });
            #endregion

        }
    }
}
