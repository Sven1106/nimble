﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Models;
using Nimble.Common.Models.Deserialization;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Nimble.Common.Events;

namespace Nimble.Common.Actors
{
    public class ProjectActor : ReceiveActor
    {
        private IActorRef BrowserRef { get; }
        private IActorRef HtmlContentGuardRef { get; }
        private IActorRef ObjectParserRef { get; }
        private IActorRef ObjectTrackerRef { get; }
        private IActorRef ProgressRef { get; }
        private IActorRef UrlTrackerRef { get; }
        private IActorRef UrlParserRef { get; }
        private ProjectDefinition ProjectDefinition { get; }
        private Guid AccountId { get; }
        private Guid ProjectId { get; }
        public ProjectActor(Guid accountId, ProjectDefinition projectDefinition)
        {
            // TODO Add some kind of uservalidation
            AccountId = accountId;
            ProjectId = projectDefinition.ProjectId;
            ProjectDefinition = projectDefinition;
            XpathConfigurationForPuppeteer crawlerConfiguration = new XpathConfigurationForPuppeteer(ProjectDefinition);
            #region Independent
            ObjectTrackerRef = Context.ActorOf(Props.Create(() => new ObjectTrackerActor(ProjectDefinition.ScraperSchemas)), NimbleActorPaths.ObjectTracker.Name); // there can NEVER be more than ONE instance!!!!
            ProgressRef = Context.ActorOf(Props.Create(() => new ProgressActor(AccountId, ProjectId)), NimbleActorPaths.Progress.Name); // there can NEVER be more than ONE instance!!!!
            #endregion


            ObjectParserRef = Context.ActorOf(Props.Create(() => new ObjectParserActor(ProjectDefinition.ScraperSchemas, AccountId, ProjectId)), NimbleActorPaths.ObjectParser.Name); // TODO Add coordinator for scaling
            UrlTrackerRef = Context.ActorOf(Props.Create(() => new UrlTrackerActor(ProjectDefinition.Domain)), NimbleActorPaths.UrlTracker.Name); // there can NEVER be more than ONE instance!!!!
            UrlParserRef = Context.ActorOf(Props.Create(() => new UrlParserActor()), NimbleActorPaths.UrlParser.Name); // TODO Add coordinator for scaling
            HtmlContentGuardRef = Context.ActorOf(Props.Create(() => new HtmlContentGuardActor(ProjectDefinition.StartUrls, ProjectDefinition.IsFixedListOfUrls)), NimbleActorPaths.HtmlContentGuard.Name);
            BrowserRef = Context.ActorOf(Props.Create(() => new BrowserActor(crawlerConfiguration)), NimbleActorPaths.Browser.Name); // TODO Add coordinator for scaling


            #region Configure actors to use their dependencies
            ObjectParserRef.Tell(new ConfigureObjectParser(ObjectTrackerRef));
            UrlTrackerRef.Tell(new ConfigureUrlTracker(BrowserRef, ProgressRef));
            UrlParserRef.Tell(new ConfigureUrlParser(UrlTrackerRef));
            HtmlContentGuardRef.Tell(new ConfigureHtmlContentGuard(ObjectParserRef, UrlParserRef));
            BrowserRef.Tell(new ConfigureBrowser(ProgressRef, HtmlContentGuardRef));
            #endregion


            UrlTrackerRef.Tell(new UnprocessedUrlsFound(ProjectDefinition.StartUrls)); // START URL
        }

        private void Ready()
        {
            Receive<Pause>(message =>
            {
                BrowserRef.Forward(message);
            });
            Receive<Start>(message =>
            {
                BrowserRef.Forward(message);
            });

            Receive<Terminate>(message =>
            {
                Self.Tell(PoisonPill.Instance);
            });
        }


        #region Lifecycle Hooks
        protected override void PreStart()
        {
            Console.WriteLine(Self.Path.Name + " was created");
            NimbleActorPaths.DirectoryRef.Tell(new AnnounceSelf(AccountId.ToString() + ProjectId.ToString()));
            NimbleActorPaths.DirectoryRef.Tell(new ProjectRegistered(AccountId, ProjectId));
            Become(Ready);
        }
        public override void AroundPostStop()
        {
            Console.WriteLine(Self.Path.Name + " was terminated");
            NimbleActorPaths.DirectoryRef.Tell(new WithdrawSelf(AccountId.ToString() + ProjectId.ToString()));
            NimbleActorPaths.DirectoryRef.Tell(new ProjectTerminated(AccountId, ProjectId));
            Context.Parent.Tell(new ProjectTerminated(AccountId, ProjectId));
        }

        protected override SupervisorStrategy SupervisorStrategy()
        {
            return new OneForOneStrategy(
                exception =>
                {
                    return Directive.Restart;
                });
        }
        #endregion
    }
}
