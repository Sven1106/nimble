﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Events;
using Nimble.Common.Models;
using System;
using System.Collections.Generic;

namespace Nimble.Common.Actors
{
    public class HtmlContentGuardActor : ReceiveActor // Directs where the htmlContent should go based on the projectdefinition 
    {
        private List<Uri> StartUrls { get; }
        private bool IsFixedListOfUrls { get; }
        private IActorRef ObjectParserRef { get; set; }
        private IActorRef UrlParserRef { get; set; }
        public HtmlContentGuardActor(List<Uri> startUrls, bool isFixedListOfUrls)
        {
            StartUrls = startUrls;
            IsFixedListOfUrls = isFixedListOfUrls;

            Configurable();
        }

        private void Configurable()
        {
            Receive<ConfigureHtmlContentGuard>(message =>
            {
                ObjectParserRef = message.ObjectParserRef;
                UrlParserRef = message.UrlParserRef;
                Become(Ready);
            });
        }

        private void Ready()
        {
            Receive<HtmlContentFound>(message =>
            {
                ObjectParserRef.Tell(message);
                if (IsFixedListOfUrls == false ||
                    IsFixedListOfUrls == true && StartUrls.Contains(message.SourceUrl)
                )
                {
                    UrlParserRef.Tell(message);
                }
            });
        }

    }
}
