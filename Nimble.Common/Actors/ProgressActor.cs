﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Events;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Actors
{
    public class ProgressActor : ReceiveActor
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        private int ProcessedUrlsCount { get; set; } = 0;
        private int ValidUrlsCount { get; set; } = 0;
        private Cancelable CancellationKey { get; set; } = new Cancelable(Context.System.Scheduler);
        public DateTime StartedAt { get; private set; } = DateTime.UtcNow;
        public int IntervalInSeconds { get; private set; } = 5;
        public int UrlsCrawledInInterval { get; private set; } = 0; // TODO Find a way to get average
        public decimal UrlsCrawledPrSecond { get { return (decimal)UrlsCrawledInInterval / IntervalInSeconds; } }
        public ProgressActor(Guid accountId, Guid projectId)
        {
            AccountId = accountId;
            ProjectId = projectId;
        }

        private void Ready()
        {
            Context.System.Scheduler.ScheduleTellRepeatedly(TimeSpan.Zero, TimeSpan.FromSeconds(IntervalInSeconds), Self, new GetProgress(), Self, CancellationKey);
            #region TODO Move this into a ProgressActor
            Receive<UrlWasProcessed>(message =>
            {
                UrlsCrawledInInterval++;
                ProcessedUrlsCount++;
            });

            Receive<GetProgress>(message =>
            {
                NimbleActorPaths.DirectoryRef.Tell(new ProjectProgress(AccountId, ProjectId, ValidUrlsCount, ProcessedUrlsCount, UrlsCrawledPrSecond, SecondsRemaining));
                UrlsCrawledInInterval = 0;
            });

            Receive<ValidUrlEvent>(message =>
            {
                ValidUrlsCount++;
            });
            #endregion
        }
        private int SecondsRemaining
        {
            get
            {
                int secondsLeft = 0;
                if (ProcessedUrlsCount != 0 && ValidUrlsCount != 0)
                {
                    TimeSpan timePassed = DateTime.UtcNow.Subtract(StartedAt);
                    int remainingUrlsCount = ValidUrlsCount - ProcessedUrlsCount;
                    secondsLeft = (int)Math.Floor(timePassed.TotalSeconds / ProcessedUrlsCount * remainingUrlsCount);
                }
                return secondsLeft;
            }
        }
        #region Lifecycle Hooks
        protected override void PreStart()
        {
            Become(Ready);
        }

        protected override void PostStop()
        {
            CancellationKey.Cancel();
        }
        #endregion

    }
}
