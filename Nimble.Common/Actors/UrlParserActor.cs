﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Models;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using Nimble.Common.Events;

namespace Nimble.Common.Actors
{
    public class UrlParserActor : ReceiveActor
    {

        private IActorRef UrlTrackerRef { get; set; }
        public UrlParserActor()
        {
            Configurable();
        }
        private void Configurable()
        {
            Receive<ConfigureUrlParser>(message => {
                UrlTrackerRef = message.UrlTrackerRef;
                Become(Ready);
            });
        }
        private void Ready()
        {
            Receive<HtmlContentFound>(htmlContent =>
            {
                List<Uri> parsedUrls = ParseUrlsFromHtmlContent(htmlContent);
                UnprocessedUrlsFound unprocessedUrls = new UnprocessedUrlsFound(parsedUrls);
                UrlTrackerRef.Tell(unprocessedUrls);
            });
        }

        private List<Uri> ParseUrlsFromHtmlContent(HtmlContentFound htmlContent)
        {
            HtmlDocument htmlDoc = new HtmlDocument();
            htmlDoc.LoadHtml(htmlContent.Html);
            List<Uri> urlsFound = new List<Uri>();
            if (htmlDoc.DocumentNode.SelectSingleNode("//urlset[starts-with(@xmlns, 'http://www.sitemaps.org')]") != null) // if sitemap
            {
                var locs = htmlDoc.DocumentNode.SelectNodes("//loc");
                if (locs != null)
                {
                    foreach (var loc in locs)
                    {
                        string value = loc.InnerText;
                        Uri url = new Uri(value, UriKind.RelativeOrAbsolute);
                        urlsFound.Add(url);
                    }
                }
            }
            else
            {
                var aTags = htmlDoc.DocumentNode.SelectNodes("//a[@href]");
                if (aTags != null)
                {
                    foreach (var aTag in aTags)
                    {
                        string hrefValue = aTag.Attributes["href"].Value;
                        hrefValue = WebUtility.HtmlDecode(hrefValue);
                        Uri url = new Uri(hrefValue, UriKind.RelativeOrAbsolute);
                        url = new Uri(htmlContent.SourceUrl, url);
                        urlsFound.Add(url);
                    }
                }
            }
            return urlsFound;
        }

        #region Lifecycle Hooks
        protected override void PreStart()
        {

        }
        #endregion
    }
}
