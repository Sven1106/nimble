﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Nimble.Common.Events;

namespace Nimble.Common.Actors
{
    public class UrlTrackerActor : ReceiveActor // TODO Should be persistent actor.
    {
        public List<Uri> ValidUrls { get; private set; } = new List<Uri>();
        private Uri Domain { get; }
        private IActorRef BrowserRef { get; set; }
        private IActorRef ProgressRef { get; set; }


        public UrlTrackerActor(Uri domain)
        {

            Domain = domain;
            Configurable();
        }
        private void Configurable()
        {
            Receive<ConfigureUrlTracker>(message => {
                BrowserRef = message.BrowserRef;
                ProgressRef = message.ProgressRef;
                Become(Ready); // Should it be here?
            });
        }
        private void Ready()
        {
           
            Receive<UnprocessedUrlsFound>(message =>
            {
                List<Uri> distinctUrls = message.Urls.Distinct().ToList(); // TODO Can ToLower() be implemented? or will it result in false positives when visiting the Urls later? And what about case sensitive urls?
                foreach (var url in distinctUrls)
                {
                    #region Checks if url is valid
                    bool isUrlFromProjectDomain = url.OriginalString.Contains(Domain.OriginalString);
                    // bool doesUrlContainAnyDisallowedWords = disallowedWords.Any(url.ToString().Contains); //TODO Add check for disallowed words?
                    if (isUrlFromProjectDomain == false ||
                        ValidUrls.Contains(url) == true)
                    {
                        continue;
                    }
                    ValidUrls.Add(url);
                    ProgressRef.Tell(new ValidUrlEvent(url));
                    #endregion
                    BrowserRef.Tell(new DownloadUrl(url)); // BrowserActor is load balancer
                }
            });
        }
        #region Lifecycle Hooks
        protected override void PreStart()
        {

        }

        protected override void PostStop()
        {

        }
        #endregion
    }
}
