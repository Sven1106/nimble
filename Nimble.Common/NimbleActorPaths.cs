﻿using Akka.Actor;
using Nimble.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace Nimble.Common
{

    public static class NimbleActorPaths
    {
        public static readonly string Nimble = "Nimble";
        public static readonly ActorMetaData Directory = new ActorMetaData("Directory", "akka.tcp://" + Nimble + "@127.0.0.1:9091"); // Top level actor
        public static readonly ActorMetaData UserManager = new ActorMetaData("UserManager", "akka.tcp://" + Nimble + "@127.0.0.1:9091"); // Top level actor
        public static readonly ActorMetaData User = new ActorMetaData("User", UserManager, true);
        public static readonly ActorMetaData Project = new ActorMetaData("Project", User, true);
        public static readonly ActorMetaData HtmlContentGuard = new ActorMetaData("HtmlContentGuard", Project, true);
        public static readonly ActorMetaData UrlParser = new ActorMetaData("UrlParser", Project);
        public static readonly ActorMetaData UrlTracker = new ActorMetaData("UrlTracker", Project);
        public static readonly ActorMetaData ObjectParser = new ActorMetaData("ObjectParser", Project);
        public static readonly ActorMetaData ObjectTracker = new ActorMetaData("ObjectTracker", Project);
        public static readonly ActorMetaData Progress = new ActorMetaData("Progress", Project);
        public static readonly ActorMetaData Browser = new ActorMetaData("Browser", Project);
        public static readonly ActorMetaData Page = new ActorMetaData("Page", Browser);
        public static IActorRef DirectoryRef;

    }

    public class ActorMetaData
    {
        public ActorMetaData(string name)
        {
            ActorMetaData parent = null;
            string protocolSystemAddress = "";
            bool isScalableAndHasCustomName = false;
            Setup(ref name, ref parent, ref protocolSystemAddress, ref isScalableAndHasCustomName);
        }
        public ActorMetaData(string name, ActorMetaData parent)
        {
            string protocolSystemAddress = "";
            bool isScalableAndHasCustomName = false;
            Setup(ref name, ref parent, ref protocolSystemAddress, ref isScalableAndHasCustomName);
        }
        public ActorMetaData(string name, string protocolSystemAddress)
        {
            ActorMetaData parent = null;
            bool isScalableAndHasCustomName = false;
            Setup(ref name, ref parent, ref protocolSystemAddress, ref isScalableAndHasCustomName);
        }
        public ActorMetaData(string name, bool isScalableAndHasCustomName)
        {
            string protocolSystemAddress = "";
            ActorMetaData parent = null;
            Setup(ref name, ref parent, ref protocolSystemAddress, ref isScalableAndHasCustomName);
        }
        public ActorMetaData(string name, ActorMetaData parent, bool isScalableAndHasCustomName)
        {
            string protocolSystemAddress = "";
            Setup(ref name, ref parent, ref protocolSystemAddress, ref isScalableAndHasCustomName);
        }



        public void Setup(ref string name, ref ActorMetaData parent, ref string protocolActorsystemAdress, ref bool isScalableAndHasCustomName)
        {
            Name = name;
            Parent = parent;

            // if no parent, we assume a top-level actor
            var parentPath = parent != null ? parent.Path : "/user";

            if (isScalableAndHasCustomName)
            {
                Path = string.Format("{0}{1}/", protocolActorsystemAdress, parentPath) + "{" + Name + "}";
            }
            else
            {
                Path = string.Format("{0}{1}/{2}", protocolActorsystemAdress, parentPath, Name);
            }
        }

        public string Name { get; private set; }

        public ActorMetaData Parent { get; set; }

        public string Path { get; private set; }

    }
}
