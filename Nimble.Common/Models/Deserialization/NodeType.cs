﻿namespace Nimble.Common.Models.Deserialization
{
    public enum NodeType
    {
        String,
        Number,
        Boolean,
        Object
    }
}
