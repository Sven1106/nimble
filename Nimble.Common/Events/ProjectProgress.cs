﻿using System;

namespace Nimble.Common.Events
{
    public class ProjectProgress
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        public int UrlsFoundCount { get; private set; }
        public int UrlsCrawledCount { get; private set; }
        public decimal UrlsCrawledPrSecond { get; private set; }
        public int SecondsRemaining { get; private set; }
        public ProjectProgress(Guid accountId, Guid projectId, int urlsFoundCount, int urlsCrawledCount, decimal urlsCrawledPrSecond, int secondsRemaining)
        {
            AccountId = accountId;
            ProjectId = projectId;
            UrlsCrawledPrSecond = urlsCrawledPrSecond;
            SecondsRemaining = secondsRemaining;
            UrlsFoundCount = urlsFoundCount;
            UrlsCrawledCount = urlsCrawledCount;
        }
    }
}
