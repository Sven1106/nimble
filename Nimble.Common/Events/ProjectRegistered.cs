﻿using System;

namespace Nimble.Common.Events
{
    public class ProjectRegistered
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        public ProjectRegistered(Guid accountId, Guid projectId)
        {
            AccountId = accountId;
            ProjectId = projectId;
        }
    }
}