﻿using System;

namespace Nimble.Common.Events
{
    public class ValidUrlEvent
    {
        public Uri Url { get; set; }
        public ValidUrlEvent(Uri url)
        {
            Url = url;
        }
    }
}
