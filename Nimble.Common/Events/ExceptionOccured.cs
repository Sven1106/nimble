﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Events
{
    public class ExceptionOccured
    {
        public Exception Exception { get; private set; }
        public ExceptionOccured(Exception exception)
        {
            Exception = exception;
        }
    }
}
