﻿using System;

namespace Nimble.Common.Events
{
    public class ProjectTerminated
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        public ProjectTerminated(Guid accountId, Guid projectId)
        {
            AccountId = accountId;
            ProjectId = projectId;
        }
    }
}