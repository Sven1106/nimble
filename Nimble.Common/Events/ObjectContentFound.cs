﻿using Newtonsoft.Json.Linq;
using System;

namespace Nimble.Common.Events
{
    public class ObjectContentFound
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        public string ScraperSchemaName { get; private set; } // For matching
        public JObject JObject { get; private set; }
        public ObjectContentFound(Guid accountId, Guid projectId, string scraperSchemaName, JObject jObject)
        {
            AccountId = accountId;
            ProjectId = projectId;
            ScraperSchemaName = scraperSchemaName;
            this.JObject = jObject;
        }
    }
}
