﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Events
{
    public class UserRegistered
    {
        public Guid AccountId { get; private set; }
        public UserRegistered(Guid accountId)
        {
            AccountId = accountId;
        }
    }
}
