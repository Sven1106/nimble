﻿using System;

namespace Nimble.Common.Events
{
    public class UrlWasProcessed
    {
        public Uri Url { get; set; }
        public UrlWasProcessed(Uri url)
        {
            Url = url;
        }
    }
}
