﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Events
{
    public class UserAuthenticated
    {
        public Guid Id { get; private set; }
        public string Email { get; private set; }
        public UserAuthenticated(Guid id, string email)
        {
            Id = id;
            Email = email;
        }
    }
}
