﻿using System;

namespace Nimble.Common.Events
{
    public class HtmlContentFound
    {
        public Uri SourceUrl { get; private set; }
        public string Html { get; private set; }

        public HtmlContentFound(Uri url, string html)
        {
            SourceUrl = url;
            Html = html;
        }
    }
}
