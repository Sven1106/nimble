﻿using System;
using System.Collections.Generic;

namespace Nimble.Common.Events
{
    public class UnprocessedUrlsFound
    {
        public List<Uri> Urls { get; private set; }
        public UnprocessedUrlsFound(List<Uri> urls)
        {
            Urls = urls;
        }
    }
}
