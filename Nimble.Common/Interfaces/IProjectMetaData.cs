﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Interfaces
{
    public interface IProjectMetaData
    {
        Guid AccountId { get; }
        Guid ProjectId { get; }
    }
}
