﻿using Akka.Actor;

namespace Nimble.Common.Interfaces
{
    public interface IProjectEventHandler
    {
        void DoProjectStuff();
    }
}