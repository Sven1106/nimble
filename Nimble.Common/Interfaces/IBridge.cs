﻿using Akka.Actor;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Interfaces
{
    public interface IBridge
    {
        List<IActorRef> EventHandlers { get; }
        IActorRef TargetActor { get; }
    }
}
