﻿using Akka.Actor;

namespace Nimble.Common.Commands
{
    public class ConfigureHtmlContentGuard
    {
        public IActorRef ObjectParserRef { get; }
        public IActorRef UrlParserRef { get; }
        public ConfigureHtmlContentGuard(IActorRef objectParserRef, IActorRef urlParserRef)
        {
            ObjectParserRef = objectParserRef;
            UrlParserRef = urlParserRef;
        }
    }
}
