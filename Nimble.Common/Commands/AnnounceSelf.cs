﻿namespace Nimble.Common.Commands
{
    public class AnnounceSelf
    {
        public string Key { get; private set; }
        public AnnounceSelf(string key)
        {
            Key = key;
        }
    }
}