﻿namespace Nimble.Common.Commands
{
    public class WithdrawSelf
    {
        public string Key { get; private set; }
        public WithdrawSelf(string key)
        {
            Key = key;
        }
    }
}