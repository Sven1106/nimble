﻿namespace Nimble.Common.Commands
{
    public class AuthenticateUser
    {
        public string Email { get; private set; }
        public string Password { get; private set; }
        public AuthenticateUser(string email, string password)
        {
            Email = email;
            Password = password;
        }
    }
}