﻿using Akka.Actor;

namespace Nimble.Common.Commands
{
    public class ConfigureUrlParser
    {
        public IActorRef UrlTrackerRef { get; }
        public ConfigureUrlParser(IActorRef urlTrackerRef)
        {
            UrlTrackerRef = urlTrackerRef;
        }
    }
}
