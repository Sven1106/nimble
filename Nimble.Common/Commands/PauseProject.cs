﻿using Nimble.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Commands
{
    public class PauseProject: IProjectMetaData
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        // TODO Add token

        public PauseProject(Guid accountId, Guid projectId)
        {
            AccountId = accountId;
            ProjectId = projectId;
        }
    }
}
