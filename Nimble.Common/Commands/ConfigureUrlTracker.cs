﻿using Akka.Actor;
using System;
using System.Collections.Generic;

namespace Nimble.Common.Commands
{
    public class ConfigureUrlTracker {
        public IActorRef BrowserRef { get; }
        public IActorRef ProgressRef { get; }
        public ConfigureUrlTracker(IActorRef browserRef, IActorRef progressRef)
        {
            BrowserRef = browserRef;
            ProgressRef = progressRef;
        }
    }

    public class ConfigureBrowser
    {
        public IActorRef ProgressRef { get; }
        public IActorRef HtmlContentGuardRef { get; }
        public ConfigureBrowser(IActorRef progressRef, IActorRef htmlContentGuardRef)
        {
            ProgressRef = progressRef;
            HtmlContentGuardRef = htmlContentGuardRef;
        }
    }
}
