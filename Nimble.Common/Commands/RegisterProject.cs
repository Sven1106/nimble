﻿using Nimble.Common.Models.Deserialization;
using System;

namespace Nimble.Common.Commands
{
    public class RegisterProject
    {
        public Guid AccountId { get; private set; }
        public string ProjectDefinitionAsJson { get; private set; }

        // TODO Add token
        public RegisterProject( Guid accountId, string projectDefinitionAsJson)
        {
            AccountId = accountId;
            ProjectDefinitionAsJson = projectDefinitionAsJson;
        }
    }
}
