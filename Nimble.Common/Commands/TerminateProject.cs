﻿using Nimble.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Commands
{
    public class TerminateProject : IProjectMetaData
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        // TODO Add token
        public TerminateProject(Guid accountId, Guid projectId)
        {
            AccountId = accountId;
            ProjectId = projectId;
        }
    }
}
