﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Nimble.Common.Commands
{
    public class RegisterUser
    {
        public Guid Id { get; private set; }
        public string Email { get; private set; }
        public string Password { get; private set; }
        public RegisterUser(string email, string password)
        {
            Id = Guid.NewGuid();
            Email = email;
            Password = password;
        }
    }
}
