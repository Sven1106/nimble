﻿using System;

namespace Nimble.Common.Commands
{
    public class DownloadUrl
    {
        public Uri TargetUrl { get; private set; }
        public DownloadUrl(Uri url)
        {
            TargetUrl = url;
        }
    }
}
