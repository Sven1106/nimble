﻿using Akka.Actor;

namespace Nimble.Common.Commands
{
    public class ConfigureObjectParser
    {
        public IActorRef ObjectTrackerRef { get; }
        public ConfigureObjectParser(IActorRef objectTrackerRef)
        {
            ObjectTrackerRef = objectTrackerRef;
        }
    }
}
