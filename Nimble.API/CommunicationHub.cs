﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Nimble.API
{

    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class CommunicationHub : Hub
    {
        private readonly NimbleApiActorSystem _nimbleApiActorSystem;
        public CommunicationHub(NimbleApiActorSystem nimbleApiActorSystem)
        {
            _nimbleApiActorSystem = nimbleApiActorSystem;
        }

        // User JWT and find information about
        public override async Task OnConnectedAsync()
        {

            //TODO Add ConnectionId to the Group where the UserId is set. Look at the token
            await Groups.AddToGroupAsync(Context.ConnectionId, Context.UserIdentifier);
            await base.OnConnectedAsync();
        }
        public override async Task OnDisconnectedAsync(Exception exception)
         {
            await Groups.RemoveFromGroupAsync(Context.ConnectionId, Context.UserIdentifier);
            await base.OnDisconnectedAsync(exception);
        }
        public Task SendMessage(string user, string message)
        {
            return Clients.All.SendAsync("ReceiveMessage", user, message);
        }
    }
}