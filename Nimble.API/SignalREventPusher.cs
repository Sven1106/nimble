﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;

namespace Nimble.API
{
    public class SignalREventPusher
    {
        private readonly IHubContext<AuthHub> _authHub;
        private readonly IHubContext<CommunicationHub> _communicationHub;
        private readonly IConfiguration _configuration;
        public SignalREventPusher(IHubContext<AuthHub> authHub, IHubContext<CommunicationHub> communicationHub, IConfiguration configuration)
        {
            _authHub = authHub;
            _communicationHub = communicationHub;
            _configuration = configuration;
        }

        public void UserRegistered( string message)
        {

        }
        public void UserAuthenticated(string id, string email)
        {
            var userToken = Helper.CreateUserToken(id, email, _configuration);
            _authHub.Clients.All.SendAsync("ReceiveUserToken", userToken); //TODO Make sure only the correct connectionId gets the userToken
        }

    }
}