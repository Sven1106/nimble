﻿using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Nimble.API
{
    public static class Helper
    {
        public static string CreateUserToken(string id,string email, IConfiguration config)
        {

            var claims = new[] {
                new Claim(ClaimTypes.NameIdentifier, id),
                //new Claim(ClaimTypes.Name, name),
                new Claim(ClaimTypes.Email, email),
                //new Claim(ClaimTypes.Role, role),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config.GetSection("SigningKey").Value));

            var credentials = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddSeconds(1),
                SigningCredentials = credentials
            };

            var jwtTokenHandler = new JwtSecurityTokenHandler();

            var createdToken = jwtTokenHandler.CreateToken(tokenDescriptor);
            return jwtTokenHandler.WriteToken(createdToken);
        }
        public static JToken BuildJsonFromIConfiguration(IConfiguration configuration)
        {
            if (configuration is IConfigurationSection configurationSection)
            {
                if (configurationSection.Value != null)
                {
                    return JValue.CreateString(configurationSection.Value);
                }
            }

            var children = configuration.GetChildren().ToList();
            if (!children.Any())
            {
                return JValue.CreateNull();
            }

            if (children[0].Key == "0")
            {
                var result = new JArray();
                foreach (var child in children)
                {
                    result.Add(BuildJsonFromIConfiguration(child));
                }

                return result;
            }
            else
            {
                var result = new JObject();
                foreach (var child in children)
                {
                    result.Add(new JProperty(child.Key, BuildJsonFromIConfiguration(child)));
                }

                return result;
            }
        }
    }
}
