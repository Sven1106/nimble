﻿using Akka.Actor;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Nimble.Common;
using Nimble.Common.Actors;
using Nimble.Common.Commands;
using Nimble.Common.Events;
using System;

namespace Nimble.API
{
    public class SignalRBridgeActor : ReceiveActor
    {

        private readonly SignalREventPusher _signalREventPusher;
        private IActorRef DirectoryRef { get; set; }
        public SignalRBridgeActor(SignalREventPusher signalREventPusher)
        {
            _signalREventPusher = signalREventPusher;
            RunTask(async () =>
            {
                DirectoryRef = await Context.ActorSelection(NimbleActorPaths.Directory.Path).ResolveOne(TimeSpan.FromSeconds(3));
                DirectoryRef.Tell(new SubscribeToEventListeners());
            });

            #region Commands
            Receive<RegisterUser>(message =>
            {
                DirectoryRef.Tell(message);
            });
            Receive<AuthenticateUser>(message =>
            {
                DirectoryRef.Tell(message);
            });

            Receive<RegisterProject>(message =>
            {
                DirectoryRef.Tell(message);
            });
            Receive<StartProject>(message =>
            {
                DirectoryRef.Tell(message);
            });
            Receive<PauseProject>(message =>
            {
                DirectoryRef.Tell(message);
            });
            Receive<TerminateProject>(message =>
            {
                DirectoryRef.Tell(message);
            });
            #endregion

            #region Events
            Receive<UserRegistered>(message =>
            {
                // TODO Send verifyUser email
                _signalREventPusher.UserRegistered("DONE");
            });

            Receive<UserAuthenticated>(message =>
            {
                _signalREventPusher.UserAuthenticated(message.Id.ToString(), message.Email);
                // TODO Create token based on data from message
            });


            //Receive<ProjectRegistered>(message =>
            //{
            //    SignalREventsPusher.ProjectCreated(message);
            //});

            //Receive<ProjectTerminated>(message =>
            //{
            //    SignalREventsPusher.ProjectTerminated(message);
            //});

            //Receive<ProjectProgress>(message =>
            //{
            //    SignalREventsPusher.ProjectProgress(message);
            //});


            //Receive<ObjectContentFound>(message =>
            //{
            //    SignalREventsPusher.ObjectContent(message);
            //});
            #endregion



        }
        public override void AroundPostStop()
        {
            DirectoryRef.Tell(new UnsubscribeFromEventListeners());
        }
    }

    public interface IEventsPusher
    {
    }
}
