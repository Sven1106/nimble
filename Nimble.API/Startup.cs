using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace Nimble.API
{
    public class Startup
    {
        public IConfiguration Configuration { get; }
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                })
                .AddJwtBearer(options =>
                {
                    options.TokenValidationParameters = new TokenValidationParameters
                    {
                        ValidateIssuerSigningKey = true,
                        IssuerSigningKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(Configuration.GetSection("SigningKey").Value)),
                        ValidateIssuer = false, //TODO Add ValidIssuer = Configuration["JwtIssuer"],
                        ValidateAudience = false //TODO Add ValidAudience = Configuration["JwtIssuer"],

                    };
                    //options.Authority = "";
                    options.Events = new JwtBearerEvents
                    {
                        OnMessageReceived = context =>
                        {
                            string authHeader = context.Request.Headers["Authorization"];
                            if (string.IsNullOrEmpty(authHeader) == false && authHeader.StartsWith("Bearer"))
                            {
                                context.Token = authHeader.Substring("Bearer ".Length).Trim();
                            }
                            var accessToken = context.Request.Query["access_token"]; // Read the token out of the query string
                            if (string.IsNullOrEmpty(accessToken) == false)
                            {
                                
                                context.Token = accessToken;
                            }
                            return Task.CompletedTask;
                        },
                        OnAuthenticationFailed = error =>
                        {
                            // How to modify the response on Failed authentication
                            //error.NoResult();
                            //error.Response.StatusCode = 500;
                            //error.Response.ContentType = "text/plain";
                            //error.Response.WriteAsync(error.Exception.ToString()).Wait();
                            return Task.CompletedTask;
                        },
                        OnTokenValidated = context =>
                        {

                            return Task.CompletedTask;
                        }
                    };
                });
            services.AddControllers();
            services.AddCors(options => // Todo. Find the optimal way to configure CORS
            {
                options.AddDefaultPolicy(builder =>
                {
                    builder
                        .AllowAnyHeader()
                        .AllowAnyMethod()
                        //.AllowAnyOrigin()
                        .SetIsOriginAllowed((host) => true)
                        .AllowCredentials();
                });
            });
            services.AddSignalR();
            services.AddSingleton<NimbleApiActorSystem>();
            services.AddSingleton<SignalREventPusher>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            app.UseRouting();
            app.UseCors();
            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<CommunicationHub>("/communication");
                endpoints.MapHub<AuthHub>("/authHub");
            });

        }
    }
}
