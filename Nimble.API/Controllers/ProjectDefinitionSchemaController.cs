﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json.Schema;
using Nimble.Common.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Nimble.API.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class ProjectDefinitionSchemaController : ControllerBase // Todo. Should this be in its own controller or should it be a static file?
    {
        private readonly string projectDefinitionSchema;
        public ProjectDefinitionSchemaController()
        {
            projectDefinitionSchema = System.IO.File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "ProjectDefinitionSchema.json"));
        }
        [HttpGet]
        public ActionResult Get()
        {
            return Content(projectDefinitionSchema, "application/json");
        }
    }
}
