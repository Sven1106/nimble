﻿using Akka.Actor;
using Microsoft.AspNetCore.SignalR;
using Nimble.Common.Commands;
using System;
using System.Threading.Tasks;

namespace Nimble.API
{
    public class AuthHub : Hub
    {
        private readonly NimbleApiActorSystem _nimbleApiActorSystem;
        public AuthHub(NimbleApiActorSystem nimbleApiActorSystem)
        {
            _nimbleApiActorSystem = nimbleApiActorSystem;
        }
        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();
        }
        public override async Task OnDisconnectedAsync(Exception exception)
        {
            await base.OnDisconnectedAsync(exception);
        }

        public void RegisterUser(string email, string password)
        {
            _nimbleApiActorSystem.SignalRBridge.Tell(new RegisterUser(email, password));
        }

        public void AuthenticateUser(string email, string password)
        {
            _nimbleApiActorSystem.SignalRBridge.Tell(new AuthenticateUser(email, password)); //TODO Add ConnectionId to Message
        }

    }
}