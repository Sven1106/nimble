﻿using Akka.Actor;
using Akka.Configuration;
using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json.Linq;
using Nimble.Common;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Nimble.API
{
    public class NimbleApiActorSystem : IAsyncDisposable
    {
        private readonly ActorSystem _actorSystem;
        public IActorRef SignalRBridge { get; private set; }

        public NimbleApiActorSystem(IConfiguration configuration, SignalREventPusher signalREventPusher)
        {
            var akkaSectionAsJson = Helper.BuildJsonFromIConfiguration(configuration.GetSection("akka")); // TODO ensure object is serializable
            var config = ConfigurationFactory.FromObject(new { akka = akkaSectionAsJson });
            _actorSystem = ActorSystem.Create(NimbleApiActorPaths.NimbleApiActorSystem, config);
            SignalRBridge = _actorSystem.ActorOf(
                Props.Create(() => new SignalRBridgeActor(signalREventPusher)), NimbleApiActorPaths.SignalRBridgeActor
            );
        }

        public async ValueTask DisposeAsync()
        {
            await _actorSystem.Terminate();
        }
    }

    public static class NimbleApiActorPaths
    {
        public static readonly string NimbleApiActorSystem = "NimbleApi";
        public static readonly string SignalRBridgeActor = "SignalRBridge";
    }
}
