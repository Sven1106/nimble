﻿using System;
using System.Threading.Tasks;
using Akka.Actor;
using Nimble.Common;
using Nimble.Common.Actors;

namespace Nimble
{
    public static class NimbleActorSystem
    {
        private static ActorSystem ActorSystem { get; set; }
        public static void Start()
        {
            ActorSystem = ActorSystem.Create(NimbleActorPaths.Nimble);
            IActorRef accountManager = ActorSystem.ActorOf(Props.Create(() => new UserManagerActor()), NimbleActorPaths.UserManager.Name);
            NimbleActorPaths.DirectoryRef = ActorSystem.ActorOf(Props.Create(() => new DirectoryActor()), NimbleActorPaths.Directory.Name);
            Console.WriteLine("NimbleActorSystem started");
        }

        public static async Task ShutdownAsync()
        {
            await ActorSystem.Terminate();
            Console.WriteLine("NimbleActorSystem terminated");
        }
    }
}
