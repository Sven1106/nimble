﻿using System;
using System.Threading.Tasks;

namespace Nimble
{
    class Program
    {
        static async Task Main(string[] args)
        {
            NimbleActorSystem.Start();
            Console.ReadLine();
            await NimbleActorSystem.ShutdownAsync();
            Console.ReadLine();
        }
    }
}
