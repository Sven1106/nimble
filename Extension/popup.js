
try {
    function getTimeStamp() {
        let d = new Date();
        return d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    }
    console.log('popup.js loaded', getTimeStamp());
    //chrome.storage.local.set({ 'userToken': Date.now() });
    // 

    var worker = new SharedWorker('sharedWorker.js');

    var workerConnectionId;

    worker.port.addEventListener('message', function (e) {
        if (e.data.type == 'CONNECTION') {
            workerConnectionId = e.data.connectionId
        } else if (e.data.type == 'ALERT') {
            alert('Ola ' + e.data.name);
        }
    });

    worker.port.start();
    let signInPageElement = document.getElementById("signInPage");
    let registerPageElement = document.getElementById("registerPage");
    let homePageElement = document.getElementById("homePage");
    // chrome.storage.local.get('userToken', (value) => {
    //     console.log(value);
    //     let userToken = value.userToken;

    //     if (userToken) {
    //         chrome.storage.local.set({'selectedPage':'homePage'});
    //     }
    //     else {
    //         chrome.storage.local.set({'selectedPage':'signInPage'});
    //     }
    // });


    chrome.storage.local.get('selectedPage', (value) => {
        let selectedPage = value.selectedPage;
        if (selectedPage) {
            changeToPage(selectedPage);
        }
        else {
            chrome.storage.local.set({ 'selectedPage': 'signInPage' });
        }
    });

    chrome.storage.local.get('connectionStatus', (value) => {
        let connectionStatus = value.connectionStatus;
        if (connectionStatus) {
            changeToConnectionStatus(connectionStatus);
        }
    });

    //TODO Get layout state on load.


    //TODO Add ConnectionLost eventListener
    //TODO Add ConnectionEstablished eventlistener

    chrome.storage.local.onChanged.addListener(async (changes) => {
        for (const key in changes) {
            switch (key) {
                case 'selectedPage':
                    let selectedPage = changes[key].newValue;
                    changeToPage(selectedPage);
                    break;
                case 'connectionStatus':
                    console.log(changes[key]);
                    let oldConnectionStatus = changes[key].oldValue;
                    let newConnectionStatus = changes[key].newValue;
                    if (newConnectionStatus != oldConnectionStatus) {
                        changeToConnectionStatus(newConnectionStatus);
                    }
                    break;
                default:
                    break;
            }
        }
    });


    signInPageElement.getElementsByTagName("form")[0].addEventListener('submit', (e) => {
        e.preventDefault();
        //TODO add validation
        let message = new Message('authenticateUser', {
            'email': e.target['email'].value,
            'password': e.target['password'].value
        });
        chrome.runtime.sendMessage(message);
    });

    registerPageElement.getElementsByTagName("form")[0].addEventListener('submit', (e) => {
        e.preventDefault();
        //TODO add validation
        let message = new Message('registerUser', {
            'email': e.target['email'].value,
            'password': e.target['password'].value
        });
        chrome.runtime.sendMessage(message);
        chrome.storage.local.set({ 'selectedPage': 'signInPage' });
    });


    for (const goToRegisterPageBtn of document.getElementsByClassName("goToRegisterPageBtn")) {
        goToRegisterPageBtn.addEventListener("click", async () => {
            chrome.storage.local.set({ 'selectedPage': 'registerPage' });
        });
    }

    for (const signOutBtn of document.getElementsByClassName("signOutBtn")) {
        signOutBtn.addEventListener("click", async () => {
            console.log('clicked');
            chrome.storage.local.remove('userToken');
        });
    }
    let connectionElement = document.getElementById('connection');
    let connectionTitleElement = connectionElement.getElementsByClassName('connectionTitle')[0];
    let connectionMessageElement = connectionElement.getElementsByClassName('connectionMessage')[0];
    let connectionOverlayElement = document.getElementById('connectionOverlay');

    function changeToConnectionStatus(connectionStatus) {
        connectionOverlayElement.classList.remove("invisible");
        switch (connectionStatus) {
            case 'connecting':
                connectionTitleElement.innerHTML = 'Connecting to server';
                connectionMessageElement.innerHTML = 'Negotiating';
                connectionMessageElement.classList.add('loading');
                changeToAnimation(connectionElement, 'connecting');
                break;
            case 'reconnecting':
                connectionTitleElement.innerHTML = 'Reconnecting to server';
                connectionMessageElement.innerHTML = 'Renegotiating';
                connectionMessageElement.classList.add('loading');
                changeToAnimation(connectionElement, 'connecting');
                break;
            case 'connected':
                connectionTitleElement.innerHTML = 'Connection established';
                connectionMessageElement.innerHTML = 'Complete';
                connectionMessageElement.classList.remove('loading');
                changeToAnimation(connectionElement, 'connected');
                break;
            case 'failed':
                connectionTitleElement.innerHTML = 'Connection failed';
                connectionMessageElement.innerHTML = 'Negotiation unsuccessful';
                connectionMessageElement.classList.remove('loading');
                changeToAnimation(connectionElement, 'error');
                triggerAnimationAndRemoveAtAnimationEnd(connectionElement, 'ahashakeheartache');
                break;
            case 'closed':
                connectionTitleElement.innerHTML = 'Connection closed';
                connectionMessageElement.innerHTML = 'Negotiation unsuccessful';
                connectionMessageElement.classList.remove('loading');
                changeToAnimation(connectionElement, 'error');
                triggerAnimationAndRemoveAtAnimationEnd(connectionElement, 'ahashakeheartache');
                //connectionElement.addEventListener('animationend', animationEndCallback);
                // TODO Promp user to reconnect?
                break;
            case 'lost':
                connectionTitleElement.innerHTML = 'Connection lost';
                connectionMessageElement.innerHTML = 'Negotiation unsuccessful';
                connectionMessageElement.classList.remove('loading');
                changeToAnimation(connectionElement, 'error');
                triggerAnimationAndRemoveAtAnimationEnd(connectionElement, 'ahashakeheartache');
                break;
            default:
                break;
        }
    }
    function changeToAnimation(element, animation) {
        element.querySelectorAll('.connectionAnimation').forEach((el) => el.classList.add('hidden'));
        element.getElementsByClassName(animation)[0].classList.remove('hidden');
    }
    function triggerAnimationAndRemoveAtAnimationEnd(element, animation) {
        element.classList.add(animation);
        element.addEventListener('animationend', animationendCallback(element, animation));
    }

    function animationendCallback(element, animation) {
        return function () {
            element.classList.remove(animation);
            element.removeEventListener('animationend', animationendCallback(element, animation));
            console.log('animationend removed');
        }
    }

    let wakeupElement = document.getElementById("wakeup");
    wakeupElement.addEventListener('click', () => wakeUp())


    function stayAwake() {
        chrome.runtime.sendMessage(new Message('wakeup'));

        // chrome.storage.local.get('stayAwake', (value) => {
        //     chrome.storage.local.set({ 'stayAwake': value.stayAwake ? false : true });
        // });
    }
    // chrome.runtime.sendMessage(new Message('wakeup', null));

    // setInterval(stayAwake, 2000)

    function changeToPage(targetPage) {
        let targetPageElement = document.getElementById(targetPage);
        if (targetPageElement) {

            let pageElements = document.querySelectorAll("[id$='Page']");
            pageElements.forEach(pageElement => {
                if (pageElement !== targetPageElement) {
                    pageElement.classList.add("hidden");
                }
            });
            targetPageElement.classList.remove("hidden");
        }
    }

    // loadScript("vue.min.js", codeToExecute);
    function loadScript(url, callback) {
        // Adding the script tag to the head as suggested before
        var head = document.head;
        var script = document.createElement('script');
        script.type = 'text/javascript';
        script.src = url;

        // Then bind the event to the callback function.
        // There are several events for cross browser compatibility.
        script.onreadystatechange = callback;
        script.onload = callback;

        // Fire the loading
        head.appendChild(script);
    }


} catch (error) {
    console.error(error);
}