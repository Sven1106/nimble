try {

  // const { Message } = require('./models/message');
  function getTimeStamp() {
    let d = new Date();
    return d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
  }
  console.log(self);
  self.onconnect = function (e) {
    var port = e.ports[0];  // get the port
    port.onmessage = function (e) {

      port.postMessage(`Message from worker ${port}`);
    }
    console.log(e.ports);

    port.start();
    const apiUrl = 'https://localhost:44347';
    const authUrl = apiUrl + '/authHub';

    self.importScripts('node_modules/@microsoft/signalr/dist/webworker/signalr.js');
    (async () => {
      let authConnection = new signalR.HubConnectionBuilder()
        .withUrl(authUrl)
        //.configureLogging(signalR.LogLevel.Trace)
        .withAutomaticReconnect()
        .build();
      port.postMessage(authUrl);
      await authConnection.start();
    })();
  }

} catch (error) {
  console.log(error);
}