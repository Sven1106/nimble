class Message {
    constructor(command, value = null) {
        this.command = command;
        this.value = value;
    }
}