//This is registered as a service worker.
//The service worker is created on every tab the chrome extension opens on.
//Service workers doesn't play nice with websockets and will go idle after 30 seconds.

try {
    // const { Message } = require('./models/message');
    function getTimeStamp() {
        let d = new Date();
        return d.getHours() + ':' + d.getMinutes() + ':' + d.getSeconds();
    }
    // import * as signalR from "@microsoft/signalr";
    (async () => {
        console.log('background.js loaded', getTimeStamp());
        // check if user is logged in.
        const apiUrl = 'https://localhost:44347';
        const communicationUrl = apiUrl + '/communication';
        const authUrl = apiUrl + '/authHub';
        self.importScripts('node_modules/@microsoft/signalr/dist/webworker/signalr.js', '/models/message.js');
        console.log(self);
        // myWorker.port.start();

        // myWorker.port.onmessage = function (e) {
        //     console.log(e);
        // }
        // self.addEventListener('install', event => {
        //     const preCache = async () => {
        //         console.log('waiting');
        //         const cache = await caches.open('static-v1');
        //         console.log(cache);
        //         return cache.addAll([
        //             '/',
        //             '/about/',
        //             '/static/styles.css'
        //         ]);
        //     };
        //     event.waitUntil(preCache());
        // });

        //TODO Configure popupLayout
        chrome.storage.local.get('userToken', async (value) => {
            let userToken = value.userToken;
            if (userToken) {
                chrome.storage.local.set({ 'selectedPage': 'homePage' });
                await createCommunicationConnection(userToken);
            }
            else {
                chrome.storage.local.set({ 'selectedPage': 'signInPage' });
                await createAndStartAuthConnection();
            }
        });

        chrome.runtime.onMessage.addListener(
            (request) => {

                switch (request.command) {
                    case "wakeup":
                        console.log(request);
                        break;
                    default:
                        break;
                }
            }
        );

        chrome.storage.local.onChanged.addListener(async (changes) => {
            console.log(changes);
            for (const key in changes) {
                switch (key) {
                    case 'userToken':
                        let newUserToken = changes[key].newValue;
                        if (newUserToken) {
                            chrome.storage.local.set({ 'selectedPage': 'homePage' });
                            await createCommunicationConnection(newUserToken);
                        }
                        else {
                            chrome.storage.local.set({ 'selectedPage': 'signInPage' });
                            await createAndStartAuthConnection();
                        }
                        break;
                    case 'stayAwake':

                        break;
                    default:
                        break;
                }
            }
        });

        async function createAndStartAuthConnection() {
            console.log('createAndStartAuthConnection');
            let authConnection = new signalR.HubConnectionBuilder()
                .withUrl(authUrl)
                //.configureLogging(signalR.LogLevel.Trace)
                .withAutomaticReconnect()
                .build();
            await startConnectionAsync(authConnection);

            authConnection.on("ReceiveUserToken", async (userToken) => {
                console.log('ReceiveUserToken received')
                console.log(userToken);
                chrome.storage.local.set({ userToken });
            });
            authConnection.onclose(() => { // only happens if connectiong was first established
                console.log('authConnection closed');
                chrome.storage.local.set({ 'connectionStatus': 'closed' });
                // TODO Promp user to reconnect?
            });
            authConnection.onreconnecting(() => {
                console.log('authConnection reconnecting');
                chrome.storage.local.set({ 'connectionStatus': 'reconnecting' });
            });
            authConnection.onreconnected(() => {
                console.log('authConnection connected');
                chrome.storage.local.set({ 'connectionStatus': 'connected' });
            });

            chrome.runtime.onMessage.addListener(
                (request) => {
                    switch (request.command) {
                        case "registerUser":
                            authConnection.send("RegisterUser", request.value.email, request.value.password);
                            break;
                        case "authenticateUser":
                            authConnection.send("AuthenticateUser", request.value.email, request.value.password);
                            break;
                        default:
                            break;
                    }
                }
            );
        }


        async function createCommunicationConnection(userToken) {
            console.log('createCommunicationConnection');
            let communicationConnection = new signalR.HubConnectionBuilder()
                .withUrl(communicationUrl,
                    {
                        accessTokenFactory: () => userToken, // replace with login request
                    }
                )
                //.configureLogging(signalR.LogLevel.Trace)
                .withAutomaticReconnect()
                .build();

            startConnectionAsync(communicationConnection);

            communicationConnection.on("ReceiveMessage", (user, message) => {
                console.log("ReceiveMessage was received")
            });

            communicationConnection.onclose(() => { // only happens if connectiong was first established
                console.log('communicationConnection closed');
                chrome.storage.local.set({ 'connectionStatus': 'closed' });
                // TODO Promp user to retry?
            });
            communicationConnection.onreconnecting(() => {
                console.log('communicationConnection reconnecting');
                chrome.storage.local.set({ 'connectionStatus': 'reconnecting' });
            });
            communicationConnection.onreconnected(() => {
                console.log('communicationConnection connected');
                chrome.storage.local.set({ 'connectionStatus': 'connected' });
            });
        }



        async function startConnectionAsync(connection) {
            chrome.storage.local.set({ 'connectionStatus': 'connecting' });
            console.log(`${connection.baseUrl} connecting`);
            let isRunning = true;
            while (isRunning) {
                try {
                    await connection.start()
                    console.log(`${connection.baseUrl} was connected`);
                    chrome.storage.local.set({ 'connectionStatus': 'connected' });
                    isRunning = false;
                } catch (err) {
                    switch (err.toString()) { // TODO Find a "better" way to handle errors
                        case 'Error': // Is this error always invalid_token?
                            console.log('invalid_token');
                            chrome.storage.local.set({ 'connectionStatus': 'lost' });
                            chrome.storage.local.remove('userToken');
                            isRunning = false;
                            break;
                        default:
                            chrome.storage.local.set({ 'connectionStatus': 'failed' });
                            console.log(err)
                            break;
                    }
                    console.error('major error');
                }
                await new Promise(r => setTimeout(r, 5000));
            }

        };



        // connect to CommunicationHub
        // try {
        //     self.importScripts('vue.global.js');
        //     console.log('vue.global.js was imported');
        //     const app = Vue.createApp({
        //         template: '<h1>Hello World</h1>'
        //     })
        //     app.mount('#app');v
        // } catch (error) {
        //     console.log(error);
        // }

        // (()=> {
        //     new Promise(() => );
        // })();

        // function stayAwake() {
        //     //chrome.runtime.sendMessage(new Message('wakeup', null));

        //     chrome.storage.local.get('stayAwake', (value) => {
        //         chrome.storage.local.set({ 'stayAwake': value.stayAwake ? false : true });
        //     });
        // }
        // // chrome.runtime.sendMessage(new Message('wakeup', null));

        // setInterval(stayAwake, 2000)
    })();
} catch (error) {
    console.error(error);
}