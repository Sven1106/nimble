﻿using Newtonsoft.Json.Schema;
using System;
using System.IO;
using System.Web.Mvc;

namespace AkkaWebcrawler.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View(JSchema.Parse(System.IO.File.ReadAllText(Path.Combine(AppContext.BaseDirectory, "bin", "ProjectDefinitionSchema.json"))));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}