﻿using AkkaWebcrawler.Web.SignalR.Events;
using Microsoft.AspNet.SignalR;
using Nimble.Common.Events;

namespace AkkaWebcrawler.Web.SignalR
{
    public class SignalREventsPusher
    {
        private static readonly IHubContext _nimbleHubContext;
        static SignalREventsPusher()
        {
            _nimbleHubContext = GlobalHost.ConnectionManager.GetHubContext<NimbleHub>();
        }
        public void ProjectCreated(ProjectRegistered message)
        {
            _nimbleHubContext.Clients.Group(message.AccountId.ToString()).projectCreated(message);
        }
        public void ProjectTerminated(ProjectTerminated message)
        {
            _nimbleHubContext.Clients.Group(message.AccountId.ToString()).projectTerminated(message);
        }
        public void ProjectProgress(ProjectProgress message)
        {
            _nimbleHubContext.Clients.Group(message.AccountId.ToString()).projectProgress(message);
        }
        public void ObjectContent(ObjectContentFound message)
        {
            _nimbleHubContext.Clients.Group(message.AccountId.ToString()).objectContent(message.JObject);
        }
    }
}