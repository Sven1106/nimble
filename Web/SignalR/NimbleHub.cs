﻿using Akka.Actor;
using Nimble.Common.Commands;
using Nimble.Common.Models;
using Nimble.Common.Models.Deserialization;
using Microsoft.AspNet.SignalR;
using Newtonsoft.Json;
using Newtonsoft.Json.Schema;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Nimble.Common;

namespace AkkaWebcrawler.Web.SignalR
{
    public class NimbleHub : Hub
    {
        public async Task RegisterAccount(Guid accountId)
        {
            await Groups.Add(Context.ConnectionId, accountId.ToString());
            SignalRActorSystem
                .SignalRBridge
                .Tell(new RegisterUser("test@emaisssl.dk","test"));
        }
        public void RegisterProject(Guid accountId, string json)
        {
            SignalRActorSystem
                .SignalRBridge
                .Tell(new RegisterProject(accountId, json));
        }
        public void StartProject(Guid accountId, Guid projectId)
        {
            SignalRActorSystem
            .SignalRBridge
            .Tell(new StartProject(accountId, projectId));
        }

        public void PauseProject(Guid accountId, Guid projectId)
        {
            SignalRActorSystem
            .SignalRBridge
                .Tell(new PauseProject(accountId, projectId));
        }

        public void TerminateProject(Guid accountId, Guid projectId)
        {
            SignalRActorSystem
            .SignalRBridge
                .Tell(new TerminateProject(accountId, projectId));
        }
    }
}