﻿using Akka.Actor;
using Akka.Configuration;
using Nimble.Common;
using Nimble.Common.Actors;
using Nimble.Common.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AkkaWebcrawler.Web.SignalR
{

    public static class SignalRActorSystem
    {
        public static ActorSystem ActorSystem { get; set; }
        private static SignalREventsPusher _signalREventsPusher;
        public static IActorRef SignalRBridge { get; set; }
        public static void Create()
        {
            _signalREventsPusher = new SignalREventsPusher();
            ActorSystem = ActorSystem.Create("SignalRActorSystem");
            SignalRBridge = ActorSystem.ActorOf(
                 Props.Create(() => new SignalRBridgeActor(_signalREventsPusher)), "SignalRBridge"
            );
        }
        public static async Task TerminateAsync()
        {
            await ActorSystem.Terminate();
        }
    }
}