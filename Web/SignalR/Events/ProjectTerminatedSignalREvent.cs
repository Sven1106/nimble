﻿using Nimble.Common.Interfaces;
using Nimble.Common.Commands;
using System;

namespace AkkaWebcrawler.Web.SignalR.Events
{
    public class ProjectTerminatedSignalREvent : IProjectMetaData
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        public ProjectTerminatedSignalREvent(Guid accountId, Guid projectId)
        {
            AccountId = accountId;
            ProjectId = projectId;
        }
    }
}