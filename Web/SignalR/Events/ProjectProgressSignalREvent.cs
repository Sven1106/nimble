﻿using System;
using Nimble.Common.Events;
using Nimble.Common.Interfaces;

namespace AkkaWebcrawler.Web.SignalR.Events
{
    public class ProjectProgressSignalREvent : IProjectMetaData
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        public int UrlsFoundCount { get; private set; }
        public int UrlsCrawledCount { get; private set; }
        public decimal UrlsCrawledPrSecond { get; private set; }
        public int SecondsRemaining { get; private set; }


        public ProjectProgressSignalREvent(Guid accountId, Guid projectId, ProjectProgress projectProgressMessage)
        {
            AccountId = accountId;
            ProjectId = projectId;
            UrlsCrawledPrSecond = projectProgressMessage.UrlsCrawledPrSecond;
            SecondsRemaining = projectProgressMessage.SecondsRemaining;
            UrlsFoundCount = projectProgressMessage.UrlsFoundCount;
            UrlsCrawledCount = projectProgressMessage.UrlsCrawledCount;
        }
    }
}
