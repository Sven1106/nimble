﻿using Nimble.Common.Interfaces;
using System;

namespace AkkaWebcrawler.Web.SignalR.Events
{
    public class ProjectRegisteredSignalREvent : IProjectMetaData
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        public ProjectRegisteredSignalREvent(Guid accountId, Guid projectId)
        {
            AccountId = accountId;
            ProjectId = projectId;
        }
    }
}