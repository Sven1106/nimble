﻿using AkkaWebcrawler.Web.SignalR.Events;
using Nimble.Common.Events;
using System;

namespace AkkaWebcrawler.Web.SignalR.Events
{
    public class ObjectContentSignalREvent
    {
        public Guid AccountId { get; private set; }
        public Guid ProjectId { get; private set; }
        public ObjectContentFound Message;

        public ObjectContentSignalREvent(Guid accountId, Guid projectId, ObjectContentFound message)
        {
            this.AccountId = accountId;
            this.ProjectId = projectId;
            this.Message = message;
        }
    }
}